﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV1
{
    //Zadatak 7
    class ToDoList: Note
    {
        List<Note> notes = new List<Note>();

        public ToDoList()
        {
        }

        public void addToList(Note note)
        {
            notes.Add(note);
        }
        public void removeFromList(Note note)
        {
            notes.Remove(note);
        }

        public Note getNote(int index)
        {
            return notes.ElementAt(index);
        }

        public string printList()
        {
            StringBuilder stringBuilder = new StringBuilder();
            foreach (Note note in notes)
            {
                stringBuilder.Append(note).Append("\n\n");
            }
            return stringBuilder.ToString();
        }
    }
}
