﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV1
{
    class Program
    {
        static void Main(string[] args)
        {
            //Zadatak 3
            Note FirstNote = new Note();
            Note SecondNote = new Note("This is a second note.", "Mihael", 5);
            Note HighPriorityNote = new Note("This is a note of highest priority", "Mihael");
            Console.WriteLine("First note: " + FirstNote.getText() + "\nAuthor: " + FirstNote.getAuthor());
            Console.WriteLine("\nSecond note: " + SecondNote.getText() + "\nAuthor: " + SecondNote.getAuthor());
            Console.WriteLine("\nHigh priority note: " + HighPriorityNote.getText() + "\nAuthor: " + HighPriorityNote.getAuthor());

            //Zadatak 7
            ToDoList notes = new ToDoList();
            for(int i = 1; i <= 3; i++)
            {
                Console.WriteLine("\nUnesite " + i + "." + " biljesku:");
                string note = Console.ReadLine();
                Console.WriteLine("Unesite " + i + "." + " autora:");
                string author = Console.ReadLine();
                Console.WriteLine("Unesite prioritet " + i + "." + " biljeske:");
                int priority = Convert.ToInt32(Console.ReadLine());
                Note newNote = new Note(note, author, priority);
                notes.addToList(newNote);
            }

            Console.WriteLine(notes.printList());

            for(int i = 2 ; i >= 0; i--)
            {
                if (notes.getNote(i).getPriority() <= 5)
                {
                    notes.removeFromList(notes.getNote(i));
                    
                }
            }
            Console.WriteLine("\nOdradio sam biljeske s najvecim prioritetom.");
            Console.WriteLine(notes.printList());
        }
    }
}
