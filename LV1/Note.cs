﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV1
{
    class Note
    {
        //Zadatak 2.
        private string text;
        private string author;
        private int priority; // Higher the number, lower the priority

        public void changeText(string text)
        {
            this.text = text;
        }

        public void changePriority(int priority)
        {
            this.priority = priority;
        }

        public string getText() { return this.text; }
        public string getAuthor() { return this.author; }
        public int getPriority() { return this.priority; }

        //Zadatak 3.
        public Note()
        {
            this.text = "Empty note.";
            this.author = "Unknown";
            this.priority = 10;
        }

        public Note(string text, string author, int priority)
        {
            this.text = text;
            this.author = author;
            this.priority = priority;
        }

        public Note(string text, string author)
        {
            this.text = text;
            this.author = author;
            this.priority = 1;
        }

        //Zadatak 4
        public string Text
        {
            get { return this.text; }
            set { this.text = value; }
        }
        public string Author
        {
            get { return this.author; }
            private set { this.author = value; }
        }
        public int Priority
        {
            get { return this.priority; }
            set { this.priority = value; }
        }

        //Zadatak 5
        public override string ToString()
        {
            return "\nText: " + this.text + "\nAuthor: " + this.author;
        }

    }
}
