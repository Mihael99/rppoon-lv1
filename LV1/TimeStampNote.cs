﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV1
{
    //Zadatak 6
    class TimeStampNote: Note
    {
        private DateTime time;

        public DateTime Time { get => time; set => time = value; }

        public TimeStampNote()
        {
            this.time = DateTime.Now;
        }
        public TimeStampNote(string text, string author, int priority): base(text, author, priority)
        {
            this.time = DateTime.Now;
        }
        public TimeStampNote(string text, string author, int priority, DateTime time): base(text, author, priority)
        {
            this.time = time;
        }

        public override string ToString()
        {
            return base.ToString() + "\nTime of creating note: " + this.time;
        }
    }
}
